const {expect} = require('@wdio/globals')
const MainPage = require('../../../pages/pages/index.page')
const ClipPage = require('../../../pages/pages/clip.page')


describe('Go to page with clip in Top Clips Section', () => {
    it('Opened in the new page clip should be equal to the clip that was clicked in the Top Clips section', async () => {
        await MainPage.open()
        await MainPage.getAgreeWithTermsModal().stepConfirmTerms();

        //get name and click the first clip
        let expectedClipName = await MainPage.getTopClipsSection().getClipName(1).getText();
        await MainPage.getTopClipsSection().stepClickTheClip(1);

        //verify that proper clip page was opened
        let actualClipName = await ClipPage.getClipName();
        await expect(actualClipName).toEqual(expectedClipName);
    })
})



