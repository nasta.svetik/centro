const {expect} = require('@wdio/globals')
const MainPage = require('../../../pages/pages/index.page')
const {hasDuplicates} = require("../../../libs/utils/arrayUtils");


describe('Main Page. Top Clips section.', () => {
    it('Clips list should be expanded by clicking SeeMore button per 10 items till 50 items are shown in total', async () => {
        await MainPage.open()
        await MainPage.getAgreeWithTermsModal().stepConfirmTerms();

        //expand list per 10 items until button is visible
        while (await MainPage.getTopClipsSection().isButtonSeeMoreExist()) {
            await MainPage.getTopClipsSection().stepExpand10MoreClips()
        }

        //verify the total amount of clips
        let expectedShownClipsAmount = 50
        let actualShownClipsAmount = await MainPage.getTopClipsSection().countShownClipsAmount()
        await expect(actualShownClipsAmount).toEqual(expectedShownClipsAmount);
    })


    it('Fully expanded clips list should be ordered(asc) and unique', async () => {
        await MainPage.open()

        await MainPage.getTopClipsSection().stepExpandAllClips()
        let shownClipsAmount = await MainPage.getTopClipsSection().countShownClipsAmount()

        // verify that clips in list are ordered by rating value
        const expectedRatingValuesList = [];
        for (let i = 1; i <= shownClipsAmount; i++) {
            expectedRatingValuesList.push(i);
        }
        const actualRatingValuesList = await MainPage.getTopClipsSection().getRatingValuesOfShownClips()
        await expect(actualRatingValuesList).toEqual(expectedRatingValuesList);

        // verify that clips in list are not duplicated
        const actualClipsNamesList = await MainPage.getTopClipsSection().getNamesOfShownClips();
        await expect(hasDuplicates(actualClipsNamesList)).toBeFalsy();

    })
})





