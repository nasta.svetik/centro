class ArrayUtils {
    static hasDuplicates(array) {
        const uniqueElements = new Set();

        for (const item of array) {
            if (uniqueElements.has(item)) {
                return true;
            }
            uniqueElements.add(item);
        }

        return false;
    }
}

module.exports = ArrayUtils;