# Centro-test-task

Welcome to Centro-test-task! This repository contains the source code and resources for Centro-test-task.

## Installation

### Clone the Repository

To get started, clone this repository to your local machine:

```bash
git clone https://gitlab.com/nasta.svetik/centro.git
```

### Install Dependencies

Navigate to the project directory and install the required dependencies using npm:

```bash
cd centro
npm install
```

## Usage

### Running Tests

You can run tests by executing the following command:

```bash
npm test
```

### Generating Reports 

To generate reports, use the following command:

```bash
npm run report
```
