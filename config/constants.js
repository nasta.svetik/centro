const BASE_URL = 'https://www.clips4sale.com/';


const WAIT_ELEMENT_TIMEOUT = 5000;


const SEEMORE_ADDED_ITEMS_AMOUNT = 10

module.exports = {
    BASE_URL,
    WAIT_ELEMENT_TIMEOUT,
    SEEMORE_ADDED_ITEMS_AMOUNT,
};