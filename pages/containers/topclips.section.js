const {$, $$} = require('@wdio/globals')
const BasePage = require('../basepage');
const {WAIT_ELEMENT_TIMEOUT, SEEMORE_ADDED_ITEMS_AMOUNT} = require("../../config/constants");

class TopClipsSection extends BasePage {

    get buttonSeeMoreClips() {
        return $("//*[@id=\"topClipsSection\"]/div[3]/button");
    }

    get listOfClipContainers() {
        return $$("//div[contains(@data-testid,'TopClips_')]")
    }

    getClipName(number) {
        return $("//*[@data-testid='TopClips_" + (number - 1) + "-clipCard-titleAnchor']");
    }

    getClipRatingValue(number) {
        return $("//*[@data-testid='TopClips_" + (number - 1) + "-clipCard-titleAnchor']/parent::div/parent::div/preceding-sibling::div");
    }

    async isButtonSeeMoreExist() {
        return await this.buttonSeeMoreClips.isExisting()
    }

    async countShownClipsAmount() {
        return await this.listOfClipContainers.length
    }

    async getRatingValuesOfShownClips() {
        let ratingValuesList = []
        let itemsAmount = await this.countShownClipsAmount()
        for (let i = 1; i <= itemsAmount; i++) {
            let ratingValueOnUI = await (this.getClipRatingValue(i).getText());
            ratingValuesList.push(parseInt(ratingValueOnUI, 10))
        }
        return ratingValuesList
    }

    async getNamesOfShownClips() {
        let clipsNamesList = []
        let itemsAmount = await this.countShownClipsAmount()
        for (let i = 1; i < itemsAmount; i++) {
            let clipNameOnUI = await (this.getClipName(i).getText())
            clipsNamesList.push(clipNameOnUI)
        }
        return clipsNamesList
    }

    async stepExpand10MoreClips() {
        let initialAmount = await this.countShownClipsAmount();
        let expectedAmount = initialAmount + SEEMORE_ADDED_ITEMS_AMOUNT;

        await this.buttonSeeMoreClips.click()

        await browser.waitUntil(async () =>
            (await this.countShownClipsAmount() === expectedAmount), {
            timeout: WAIT_ELEMENT_TIMEOUT,
            timeoutMsg: 'Expected' + expectedAmount + ' clips to be displayed after clicking the button, but was ' + await this.countShownClipsAmount()
        })
    }

    async stepExpandAllClips() {
        while (await this.isButtonSeeMoreExist()) {
            await this.stepExpand10MoreClips()
        }
    }

    async stepClickTheClip(number) {
        return await this.getClipName(number).click();
    }


}

module.exports = new TopClipsSection();
