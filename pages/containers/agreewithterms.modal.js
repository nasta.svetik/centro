const {$} = require('@wdio/globals')
const BasePage = require('../basepage');

class AgreeWithTermsModal extends BasePage {
    get checkboxIsAgeMore18() {
        return $("//input[@id='iCertifyCheck']/following-sibling::div")
    }

    get buttonEnter() {
        return $('#enter-agree');
    }

    get buttonLeave() {
        return $("//span[. = \'Leave\']//span");
    }

    async stepConfirmTerms() {
        await this.checkboxIsAgeMore18.click();
        await this.buttonEnter.click();
    }

}

module.exports = new AgreeWithTermsModal();
