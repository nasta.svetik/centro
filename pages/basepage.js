const {browser} = require('@wdio/globals')
const {BASE_URL} = require("../config/constants");


module.exports = class BasePage {

    open(path) {
        browser.maximizeWindow().then(() => true)
        return browser.url(BASE_URL + path)
    }
}
