const {$} = require('@wdio/globals')
const BasePage = require('../basepage');
const {WAIT_ELEMENT_TIMEOUT} = require("../../config/constants");

class ClipPage extends BasePage {

    get clipName() {
        return $("//*[@id=\"content\"]/div[2]/div[1]/figure/figcaption/div/p")
    }

    async getClipName() {
        await this.clipName.waitForExist({
            timeout: WAIT_ELEMENT_TIMEOUT, reverse: false,
            timeoutMsg: 'Clip name did not appeared'
        });
        return await this.clipName.getText()
    }

}

module.exports = new ClipPage();
