const BasePage = require('../basepage');
const AgreeWithTermsModal = require('../containers/agreewithterms.modal');
const TopClipsSection = require('../containers/topclips.section');

class IndexPage extends BasePage {

    getAgreeWithTermsModal() {
        return AgreeWithTermsModal;
    }

    getTopClipsSection() {
        return TopClipsSection;
    }

    open() {
        return super.open("");
    }
}

module.exports = new IndexPage();
